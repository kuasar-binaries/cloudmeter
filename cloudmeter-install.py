#!/usr/bin/python3
#
#Author: Ricardo Pertuz, 2022
#Version: 1.0
#Script to Install and Config Cloudmeter Agent
#curl -o ~/cloudmeter-install.py https://gitlab.com/api/v4/projects/39738443/repository/files/cloudmeter-install.py/raw?ref=main && chmod +x ~/cloudmeter-install.py && sudo ./cloudmeter-install.py
from optparse import OptionParser
import getpass
import os
import requests
import subprocess

def download(url: str, dest_folder: str, real_name: str):
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)  # create folder if it does not exist

    #filename = url.split('/')[-1].replace(" ", "_")  # be careful with file names
    file_path = os.path.join(dest_folder, real_name)

    r = requests.get(url, stream=True)
    if r.ok:
        print("saving to", os.path.abspath(file_path))
        with open(file_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024 * 8):
                if chunk:
                    f.write(chunk)
                    f.flush()
                    os.fsync(f.fileno())
    else:  # HTTP status code 4XX/5XX
                print("Download failed: status code {}\n{}".format(r.status_code, r.text))


#Arguments Logic, use for define differents options
archcommand=["uname", "-m"]
arch=subprocess.check_output(archcommand, stderr=subprocess.DEVNULL)
arch=arch.decode("utf-8").strip()
oscommand=["which", "apt"]
ostype=subprocess.run(oscommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE).returncode

if arch == 'x86_64' :
    arch='amd64'
    print('Selecting amd64')
elif arch == 'arm64' :
    arch='arm64'
    print('Selecting arm64')
else:
    arch == 'unknown'
    print('Unknown Arch')

#Download Needed Files

if arch !='unknown' :
    try:
        download("https://gitlab.com/api/v4/projects/39738443/repository/files/cloudmeter-agent.service/raw?ref=main", dest_folder="/etc/systemd/system/", real_name="cloudmeter-agent.service")
        download("https://gitlab.com/api/v4/projects/39738443/repository/files/cloudmeter-linux-config.yaml/raw?ref=main", dest_folder="/etc/", real_name="cloudmeter-config.yaml")
    except Exception as e:
        print (e)
    else:
        print('Config Files succesffully downloaded')
        downloaded=True

if arch == "amd64" :
    try:
        download("https://gitlab.com/api/v4/projects/39738443/repository/files/binaries%2Famd64%2Fcloudmeter-agent-linux-amd64/raw?ref=main", dest_folder="/usr/local/bin", real_name="cloudmeter-agent")
    except Exception as e:
        print (e)
    else:
        print('Agent succesffully downloaded')
        downloaded=True

elif arch == "arm64" :
    try:
        download("https://gitlab.com/api/v4/projects/39738443/repository/files/binaries%2Farm64%2Fcloudmeter-agent-linux-arm64/raw?ref=main", dest_folder="/usr/local/bin", real_name="cloudmeter-agent")
    except Exception as e:
        print (e)
    else:
        print('Agent succesffully downloaded')
        downloaded=True

else:
    print('Not Agent Downloaded, OS not Supported')
    downloaded=False


if downloaded :
    try: 
        username=input("username: ")
        pwd=getpass.getpass("password: ")

        # Set the values
        with open('/etc/cloudmeter-config.yaml', 'r') as file :
            set_OrgID = file.read()
            set_OrgID = set_OrgID.replace('          "X-Scope-OrgID":', '          "X-Scope-OrgID": '+username)

        with open('/etc/cloudmeter-config.yaml', 'w') as file:
            file.write(set_OrgID)
        
        with open('/etc/cloudmeter-config.yaml', 'r') as file :
            set_username = file.read()
            set_username = set_username.replace('          username:', '          username: ' +username)
        with open('/etc/cloudmeter-config.yaml', 'w') as file:
            file.write(set_username)

        with open('/etc/cloudmeter-config.yaml', 'r') as file :
            set_password = file.read()
            set_password = set_password.replace('          password:', '          password: ' +pwd)
        with open('/etc/cloudmeter-config.yaml', 'w') as file:
            file.write(set_password)

        with open('/etc/cloudmeter-config.yaml', 'r') as file :
            set_tenant = file.read()
            set_tenant = set_tenant.replace('        tenant_id:', '        tenant_id: ' +username)
        with open('/etc/cloudmeter-config.yaml', 'w') as file:
            file.write(set_tenant)           

    except Exception as e:
        print (e)
    else:
        print('')
        subprocess.call('chmod +x /usr/local/bin/cloudmeter-agent', shell=True)
        subprocess.call('systemctl daemon-reload', shell=True)
        subprocess.call('systemctl enable cloudmeter-agent', shell=True)
        subprocess.call('systemctl restart cloudmeter-agent', shell=True)
        print('Cloudmeter Agent initiated succesfully')
        subprocess.call('systemctl status cloudmeter-agent', shell=True)
     