# cloudmeter agent

## Descargar script de instalación
```bash
#Descargamos el script
curl -o cloudmeter-install https://gitlab.com/api/v4/projects/39738443/repository/files/binaries%2Famd64%2Fcloudmeter-install.py/raw?ref=main
chmod +x cloudmeter-install
#Ejecutamos el script de instalación
sudo ./cloudmeter-install --init amd64
#Se debe ingresar en username y password en Cloudmeter
username:
password:
```

## Validación status
sudo systemctl status cloudmeter-agent.service
journalctl -f -u cloudmeter-agent.service

